dataset=datasets/TAC
exp=Yago.exp15.hamacher.0.fuzzy
for ll in two three four
do
	echo $ll
	echo SUM\;MAX\;SUM_MAX
	SUM=$(( python eval.py -tr $dataset/tac16.$exp.$ll.eval.tsv -te $dataset/tac17.$exp.$ll.tsv -c REG -f 0,1,2) 2>&1 )
	MAX=$(( python eval.py -tr $dataset/tac16.$exp.$ll.eval.tsv -te $dataset/tac17.$exp.$ll.tsv -c REG -f 0,1,3,4,5 ) 2>&1 )
	SUM_MAX=$(( python eval.py -tr $dataset/tac16.$exp.$ll.eval.tsv -te $dataset/tac17.$exp.$ll.tsv -c REG -f 0,1,2,3,4,5 ) 2>&1 )
	echo $SUM\;$MAX\;$SUM_MAX
done
