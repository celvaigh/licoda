
# Reasoning with RDF KBs

## Configuration des exps:
```python
  - Base de connaissance : BaseKB, Yago, Yago après saturation
  - Agrégateurs des mesures de relatedness pour les mentions d'un document : SUM, MAX, SUM_MAX
  - Taille des chemins: 1 à 4 pour Yago et 1 à 3 pour BaseKB
```
## Agrégateurs flous avec pondération  (EXP1) : 
```python
  - L'évolution de la f1 en fonction des tailles du chemins en agrégeant les chemins de taille différentes avec le classifier
  - L’intérêt c'est de voir si la f1-score diminue si la taille des chemins augmente, en espérant que le classifier va mettre des poids faibles sur les chemins longs. 
  - Résultats : 
        La f1 score ne fait que augmenter pour SUM en restant quasi-constante pour les chemins plus grand que 3.
        Pour MAX et SUM_MAX, la f1 score a un minimum local à 3 puis continue à croitre pour 4.
        Cette exp est configurée pour maximiser la f1 score si des chemins existent entre deux entités peu importe la taille du chemin, et c'est ce qu'on observe, 
        globalement on peut s'arranger pour ajouter des chemins long en utilisant le classifier sans dégrader les performances. Ces remarques sont concordantes pour toutes les KBs.
```
## Atténuation des chemins (EXP2):  
```python
  - L'évolution de la f1 en fonction des tailles du chemins en atténuant les chemins de taille différentes par des inverses de puissances d'une certaine constante (lambda \in [1,2]).
  - L’intérêt c'est de voir si la f1-score diminue si la taille des chemins augmente, en espérant que l'atténuation des chemins longs (avec des puissances de lambda grandes) permet de 
  contrôler la qualité des chemins et surtout éviter d'introduire du bruit au chemins court qui font plus sens.
  - Résultats : 
        La f1 score augmente pour les faibles valeurs de lambda et se stabilise si lambda devient grande, mais vu que la configuration ici est similaire à EXP1, pour des petites 
        valeurs de lambda. 
        Cette exp est configurée pour globalement similaire à EXP1 pour les petites valeurs de lambda, elle permet néanmoins d'atténuer la contribution des chemins longs pour lambda 
        grande (stabilisation à partir de chemins de taille 3 et sur Yago, l'ajout des chemins de taille 4 dégrade la qualité).
```
## Calcul du pourcentage de contribution de chemins (EXP3): 
```python
  - Calcul d'un coefficient (entre 0 et 1 ) permettant de maximiser la f1 score pour les chemins de taille 2, 3 et 4, on suppose que tous les chemins de taille sont utile (C1=1). 
  - L’intérêt c'est de voir si la f1-score diminue si la taille des chemins augmente, en espérant que le contrôle du pourcentage ajouté des chemins, permet de contrôler la qualité
  des chemins et surtout éviter d'introduire du bruit au chemins court qui font plus sens.
  - Résultats : 
        Pour BaseKB, C2=1 et C3=0.8 ce qui veut dire que le meilleur score est obtenu en prenant, tous les chemins de taille 1 (logique), tous les chemins de taille 2 et 80% des chemins de taille 3.
        Ceci est probablement dû au fait que BaseKB contient pas mal de redondance dans les liens entre entités (pas d'ontologies et les mêmes faits sont exprimés différemment).
        Pour Yago et sa version saturée, C2=1, C3=0.2 et C4=0, ce qui veut dire qu'il faut prendre tous les chemins de taille 2, 20% des chemins de taille 3 et aucun chemin de taille 4. 
        Ce qui important à noter dans cette exp, c'est la décroissance du pourcentage de chemins à prendre pour 3 et 4.
```
## Agrégateurs flous sans pondération (EXP4): 
```python
  - L'évolution de la f1 en fonction des tailles du chemins en agrégeant les chemins de taille différentes avec les agrégateurs floues.
  - L’intérêt c'est de voir si la f1-score diminue si la taille des chemins augmente, en espérant que l'agrégation des chemins longs (avec des scores faibles donc) dégrade la qualité 
  des chemins court qui font plus sens.
  - Résultats : 
        La f1 score à un maximum global à 2 puis chute pour les chemins de taille 3 (BaseKB) et les chemins de taille 3 et de taille 4 (Yago, Yago saturée) pour tous (MAX, SUM, SUM_MAX).
        Cette exp est configurée pour maximiser la f1 score en se basant sur les opérateurs foules ET/OU et leurs sémantique, il y a donc une seule mesure de relatedness qui intègre 
        tous les chemins et globalement elle nous montre qu'ajouter les chemins long dégrade la qualité des liens  sémantique qui sont direct voir au plus de taille 2.
        Ces remarques sont concordantes pour toutes les KBs.
```
## Saturation
  - L'impact de la saturation sur les chemins
  
Conclusions: 

