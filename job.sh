#!/bin/bash

#OAR -n buildcorpus for EL
#OAR -p host='igrida-rolex.irisa.fr'
# -l /cpu=2,walltime=168:00:00
# -l {mem_cpu >80*1024},walltime=48:00:00
#OAR -l /cpu=2,walltime=48:00:00
#OAR -O igrida.%jobid%.output
#OAR -E igrida.%jobid%.output

#export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64/
#python NIFgenerator.py
. /etc/profile.d/modules.sh

module load pytorch/1.0.1-py3.6
module load cuda/9.0.176
python Licoda.py
