loc=0,1
sum1=2
max1=3,4,5
sum2=6
max2=7,8,9
sum3=10
max3=11,12,13
sum4=14
max4=15,16,17
wsrm1=$loc,$sum1,$max1
wsrm2=$wsrm1,$sum2,$max2
wsrm3=$wsrm2,$sum3,$max3
wsrm4=$wsrm3,$sum4,$max4
exp=exp15.yago.2.p1.p2.p3.p4
#YagoSaturated.exp15.p1.p2.p3.p4
dataset=datasets/TAC/
#REG RF MLP GB KERAS
metric=REF

for model in REG
do
	#echo $model
	for i in $loc $loc,$sum1 $loc,$max1 $wsrm1 $wsrm1,$sum2 $wsrm1,$max2 $wsrm2 $wsrm2,$sum3 $wsrm2,$max3 $wsrm3 $wsrm3,$sum4 $wsrm3,$max4 $wsrm4
	do
	echo features,$i
	#python classification.py -tr tac16.$exp.hamacher.0.p1.p2.p3.eval.tsv -te tac17.$exp.hamacher.0.p1.p2.p3.tsv -c $model -f $i
	#python classification.py -tr tac16.$exp.ref.hamacher.0.p1.p2.p3.eval.tsv -te tac17.$exp.ref.hamacher.0.p1.p2.p3.tsv -c $model -f $i
	python eval.py -tr $dataset/tac16.$exp.eval.tsv -te $dataset/tac17.$exp.tsv -c $model -f $i
	done
done
