#!/bin/bash
length=four
dataset=datasets/TAC
C=C3
echo $C\;SUM\;MAX\;SUM_MAX
for i in 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0
do 
	SUM=$(( python eval.py -tr $dataset/tac16.YagoSaturated.exp15.hamacher.0.prox2.$length.$i.eval.tsv -te $dataset/tac17.YagoSaturated.exp15.hamacher.0.prox2.$length.$i.tsv -c REG -f 0,1,2) 2>&1 )
	MAX=$(( python eval.py -tr $dataset/tac16.YagoSaturated.exp15.hamacher.0.prox2.$length.$i.eval.tsv -te $dataset/tac17.YagoSaturated.exp15.hamacher.0.prox2.$length.$i.tsv -c REG -f 0,1,3,4,5 ) 2>&1 )
	SUM_MAX=$(( python eval.py -tr $dataset/tac16.YagoSaturated.exp15.hamacher.0.prox2.$length.$i.eval.tsv -te $dataset/tac17.YagoSaturated.exp15.hamacher.0.prox2.$length.$i.tsv -c REG -f 0,1,2,3,4,5 ) 2>&1 )
	echo $i\;$SUM\;$MAX\;$SUM_MAX
done
