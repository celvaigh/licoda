#!/bin/bash

dataset=datasets/TAC
for ll in one two three four
do
	echo "###################" $ll "####################"
	echo LAMBDA\;SUM\;MAX\;SUM_MAX
	for i in 1.01 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 #1.0
	do
		SUM=$(( python eval.py -tr $dataset/tac16.YagoSaturated.exp15.hamacher.0.prox1.$ll.$i.eval.tsv -te $dataset/tac17.YagoSaturated.exp15.hamacher.0.prox1.$ll.$i.tsv -c REG -f 0,1,2) 2>&1 )
		MAX=$(( python eval.py -tr $dataset/tac16.YagoSaturated.exp15.hamacher.0.prox1.$ll.$i.eval.tsv -te $dataset/tac17.YagoSaturated.exp15.hamacher.0.prox1.$ll.$i.tsv -c REG -f 0,1,3,4,5 ) 2>&1 )
		SUM_MAX=$(( python eval.py -tr $dataset/tac16.YagoSaturated.exp15.hamacher.0.prox1.$ll.$i.eval.tsv -te $dataset/tac17.YagoSaturated.exp15.hamacher.0.prox1.$ll.$i.tsv -c REG -f 0,1,2,3,4,5 ) 2>&1 )
		echo $i\;$SUM\;$MAX\;$SUM_MAX
	done
done
