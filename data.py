import tqdm
import matplotlib
matplotlib.use('Agg')

import os,glob,gzip

import matplotlib.pyplot as plt


import gzip,math
import matplotlib
matplotlib.use('Agg')
import pylab as plt
import numpy as np
import os,sys,time
#import pandas as pd
import scipy.sparse
import tqdm
from itertools import islice
import networkx as nx

class Metrics(object):
    def __init__(self, path,ontolgypath=None,plotname=None,output="output.npy",verbose=0):
        self.path=path
        self.verbose=verbose
        self.output=output
        self.plotname=plotname
        self.dic={}
        self.entities={}
        self.ontolgypath=ontolgypath
        self.name="Metric"
        self.graphfile="graph.list"
        self.edge_labels={}
        self.yids2num=np.load("Dicts/mid2numsdic.npy",allow_pickle=True).item()
        print("finished reading ids, found : {}".format(len(self.yids2num)))
        self.make_grapgh()
    def make_grapgh(self):
        with open(self.path) as f:data=f.read().split("\n")
        edges=[]
        idx={}
        for i in tqdm.tqdm(data):
            if "prefix" in i:continue
            try:
                s,r,o,_=i.split()
                if "rdf:type" in r or "prefix" in i:continue
                if "/" in s:s=s.rsplit("/")[-1]
                if "/" in o:o=o.rsplit("/")[-1]
                for c in ["<",">","ressource:"]:s,o=s.replace(c,""),o.replace(c,"")
                sid,oid=self.yids2num[s],self.yids2num[o]
                edges+=["\t".join([str(sid),str(oid)])]#[str(idx[si]),str(idx[oi])])]
                try:self.edge_labels[(sid,oid)]+=[r]
                except:self.edge_labels[(sid,oid)]=[r]
            except:continue
        
        with open("graph.list","w") as f:f.write("\n".join(edges))
        
    def compute(self):return None
    def plot(self):return None
class Exclusivity(Metrics):
    def compute(self):
        tails={}
        heads={}
        facts=0
        EXk={}
        self.G = nx.read_edgelist(self.graphfile, nodetype=int, create_using=nx.DiGraph())
        for edge in tqdm.tqdm(self.G.edges()):self.G[edge[0]][edge[1]]['weight'] = 1
        self.G = self.G.to_undirected()
        entities={}
        print("finished reading graph {}".format(self.graphfile))
        with open(self.path) as f: data=f.read().split("\n")
        for i in tqdm.tqdm(data):
            try:s,r,o,_=i.split();facts+=1
            except:continue
            if "rdf:type" in r or "prefix" in i:continue
            if "/" in s:s=s.rsplit("/")[-1]
            if "/" in o:o=o.rsplit("/")[-1]
            for c in ["<",">","ressource:"]:s,o=s.replace(c,""),o.replace(c,"")
            sid,oid=self.yids2num[s],self.yids2num[o]
            try:heads[(sid,r)]+=1
            except:heads[(sid,r)]=1
            try:tails[(r,o)]+=1
            except:tails[(r,oid)]=1
            entities[(sid,oid)]=1
        for i in tqdm.tqdm(data):
            try:s,r,o,_=i.split();facts+=1
            except:continue
            if "rdf:type" in r or "prefix" in i:continue
            if "/" in s:s=s.rsplit("/")[-1]
            if "/" in o:o=o.rsplit("/")[-1]
            for c in ["<",">","ressource:"]:s,o=s.replace(c,""),o.replace(c,"")
            sid,oid=self.yids2num[s],self.yids2num[o]
            try:self.dic[(sid,r,oid)]=heads[(sid,r)]+tails[(r,oid)]-1
            except:pass
        print("finished computing exclusivity")
        print("tails:{} and hedas:{} and dic:{}".format(len(tails),len(heads),len(self.dic)))
        def allpath(source, target): return list(nx.all_simple_paths(self.G, source, target))
        def k_shortest_paths(source, target, k, weight=None):return list(islice(nx.shortest_simple_paths(self.G, source, target), k))
        if not len(self.dic):exit()
        def relexl(x,y,k,alpha=0.25):
            paths=k_shortest_paths(x,y,k)#allpath(x,y)[:k]
            rel=0
            for path in paths:
                s=0
                for i in range(len(path)-1):
                    try:rels=self.edge_labels[(path[i],path[i+1])]
                    except:rels=[]
                    for ri in rels:
                        s+=self.dic[(x,ri,y)]
                if s:rel+=pow(alpha,len(path))/s
            return rel
        k=5
        print("Computing top-k paths...")
        for xy in tqdm.tqdm(entities):
            x,y=xy
            EXk[(x,y)]=relexl(x,y,k)
        np.save(self.output,EXk)
def toglove(entity2id,entity2vec):
    e2i={}
    with open(entity2id) as f:data=f.read().split("\n")
    for i in tqdm.tqdm(data):
        try:idx,x=i.split("\t")
        except:continue 
        e2i[int(idx)]=x
    idx=0
    embeddings={}
    yid2wid=np.load("Dicts/yid2wid.npy").item()
    with open(entity2vec) as f:data=f.read().split("\n")
    for vec in tqdm.tqdm(data):
        
        vec=vec.split()
        if len(vec)==100:
            try:
                yid=(e2i[idx].replace("<","")).replace(">","")
                embeddings[yid2wid[yid]]=np.array(map(float,vec))
            except:pass
        else:print(vec)
        idx+=1
    print("entities found : {}".format(len(embeddings)))
    np.save("Dicts/transe_yago.npy",embeddings)


def process(filename):
    data=[]
    if os.path.isfile(filename):
        for triple in tqdm.tqdm(open(filename)):
            try:s,r,o,_=triple.split("\t")
            except:pass
            if "type" in r:continue
            data+=[triple]
    else:
        
        for file in tqdm.tqdm(glob.glob(filename+"/links-m*")):
            for triple in gzip.open(file,"rb"):
                try:
                    s,r,o,_=triple.split("\t")
                    if "http://rdf.basekb.com/ns/m." in s and "http://rdf.basekb.com/ns/m." in o:data+=[triple]
                except:pass
    print("triples found {}".format(len(data)))
    with open("data.triples","w") as f:
        f.write("\n".join(data))
    
    ds,dr={},{}
    train=[]
    for line in tqdm.tqdm(data):
        try:s,r,o=line.split("\t")
        except:
            continue#print(line.split(","))
        o=" ".join(o)
        try:tmp=ds[s]
        except:ds[s]=len(ds)
        try:tmp=dr[r]
        except:dr[r]=len(dr)
        try:tmp=ds[o]
        except:ds[o]=len(ds)
        train+=[" ".join(map(str,[ds[s],ds[o],dr[r]]))]
        
    entities=[str(ds[i])+"\t"+i for i in ds]
    relations=[str(dr[i])+"\t"+i for i in dr]
        
    with open("relation2id.txt","w") as f:f.write("\n".join(map(str,[len(dr)]+relations)))
    with open("entity2id.txt","w") as f:f.write("\n".join(map(str,[len(ds)]+entities)))
    with open("train2id.txt","w") as f:f.write("\n".join([str(len(train))]+train))

def umap_plot(filename,output):
    import umap.umap_ as UMAP
    labels = []
    tokens = []
    dic={}
    with open(filename) as f:
        data=f.read().split("\n")
    for i in tqdm.tqdm(range(len(data))):
        x=[float(k) for k in data[i].split("\t")[:-1]]
        if not len(x):continue
        tokens.append(x)
        labels.append(i)
    model = UMAP.UMAP(n_neighbors = 25, min_dist = 0.0, n_components = 3,a=1.5,b=2,verbose = True)
    p_umap = model.fit_transform(tokens)
    plt.figure(figsize=(10,10))
    plt.scatter(p_umap[:, 0], p_umap[:, 1],c=[x for x in range(len(labels))],edgecolors='gray',alpha=0.8,s=50,cmap='Spectral')
    plt.axis('off')
    plt.savefig(output)


def main():
    #process("data.triples")#/nfs/nas4/cicoda_tmp/cicoda_tmp/ldcdata/LDC2015E42_TAC_KBP_Knowledge_Base_II-BaseKB/data/")#yagoFacts.ttl")
    #umap_plot("entity2vec.vec","entity2vec.png")
    #toglove("entity2id.txt","entity2vec.vec")
    exk=Exclusivity('/nfs/nas4/cicoda/cicoda/yagoFacts.ttl',"Ditcs/yago.ex.npy",verbose=1)
    exk.compute()
if __name__ == "__main__":main()
