import warnings
warnings.filterwarnings('ignore')
import math
from sklearn.linear_model import LogisticRegression
import sklearn_crfsuite
from sklearn_crfsuite import scorers
from sklearn_crfsuite import metrics
import pandas as pd
from sklearn.svm import SVC

import matplotlib
matplotlib.use('Agg')
import pylab as plt

import itertools
from sklearn.metrics import confusion_matrix
from sklearn.ensemble import AdaBoostClassifier,VotingClassifier
from sklearn.tree import DecisionTreeClassifier
from joblib import dump, load
from sklearn.metrics import precision_recall_fscore_support,f1_score,precision_score,recall_score,roc_auc_score,average_precision_score
from sklearn.metrics import accuracy_score
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.datasets import make_gaussian_quantiles
import numpy as np
from gensim.models import KeyedVectors
from scipy import spatial
from sklearn import ensemble
from multiprocessing import  Value,Process,Pipe
from sklearn.metrics import roc_curve, auc

v1 = Value('i', 1)
v2 = Value('i', 1)
import sys

# fix random seed for reproducibility
seed = 7
np.random.seed(seed)

from itertools import chain, combinations
from matplotlib.colors import ListedColormap
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_moons, make_circles, make_classification
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis

from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree
import numpy as np
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
def union(l1, l2): 
	l3=[]
	for i in l1:l3+=[i]
	for i in l2:
		if i not in l1:l3+=[i]
	return l3
def intersection(lst1, lst2): 
    lst3 = [value for value in lst1 if value in lst2] 
    return lst3 
    
def apk(actual, predicted, k=100):
    if len(predicted)>k:
        predicted = predicted[:k]
    score = 0.0
    num_hits = 0.0
    for i,p in enumerate(predicted):
        if p in actual and p not in predicted[:i]:
            num_hits += 1.0
            score += num_hits / (i+1.0)
    if not actual:
        return 0.0
    return score / min(len(actual), k)

def mapk(actual, predicted, k=100):return np.mean([apk(a,p,k) for a,p in zip(actual, predicted)])

	
def makesample(filename):
	with open(filename,"r") as f:lines=[i.split("\t")[:8] for i in f.read().split("\n") if len(i.split("\t"))>5]
	lines=sorted(lines,key=lambda x: (x[3].split(":")[0],int(x[3].split(":")[1].split("-")[0])))
	it= itertools.groupby(lines,key=lambda x:x[3].split(":")[0])
	mentions=[]
	for i in it:mentions+=[m for m in i[1]]
	return mentions
		
class Classifier:
	def __init__(self, name,trainfile,testfile,validfile,features,embeddingfile=None):
		self.name=name
		self.embeddingfile=embeddingfile
		self.features=features
		self.X_train,self.y_train=self.getData(trainfile,self.features,self.embeddingfile)
		self.X_test,self.y_test=self.getData(testfile,self.features,self.embeddingfile)
		if validfile!=None:self.X_valid,self.y_valid=self.getData(validfile,self.features,self.embeddingfile)
		self.plot_model=False
		self.model=self.Model()
	def Model(self):
		if self.name=="KERAS":
			model = Sequential()
			_dim=len(self.X_train[0])
			#First Hidden Layer
			model.add(Dense(4, activation='relu', kernel_initializer='random_normal', input_dim=_dim))
			#Second  Hidden Layer
			_dim2=_dim/2
			model.add(Dense(4, activation='relu', kernel_initializer='random_normal'))
			#Output Layer
			model.add(Dense(1, activation='sigmoid', kernel_initializer='random_normal'))
			#model.add(Dropout(0.2))
			# Compile model
			model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
			#if self.validfile!=None:model.fit(np.array(self.X_train), np.array(self.y_train), batch_size=32,epochs=5,shuffle=True,verbose=0,validation_data=(np.array(self.X_valid),np.array(self.y_valid)))
			history=model.fit(np.array(self.X_train), np.array(self.y_train), batch_size=10,epochs=5,shuffle=True,verbose=0)
			
			if self.plot_model:
				#print(history.history.keys())
				ff=".".join(map(str,self.features))
				#  "Accuracy"
				plt.plot(history.history['acc'])
				#plt.plot(history.history['val_acc'])
				plt.title('model accuracy')
				plt.ylabel('accuracy')
				plt.xlabel('epoch')
				plt.legend(['train', 'validation'], loc='upper left')
				plt.savefig(ff+".model.accuracy.png")
				# "Loss"
				plt.plot(history.history['loss'])
				#plt.plot(history.history['val_loss'])
				plt.title('model loss')
				plt.ylabel('loss')
				plt.xlabel('epoch')
				plt.legend(['train', 'validation'], loc='upper left')
				plt.savefig(ff+".model.loss.png")
		else:
			if self.name=="QDA":model = QuadraticDiscriminantAnalysis()
			elif self.name=="KNN":model = KNeighborsClassifier(3)
			elif self.name=="MLP":model = MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(5, 2), random_state=1)
			elif self.name=="GB":
				params = dict({'n_estimators': 1000, 'max_leaf_nodes': 4, 'max_depth': None, 'random_state': 2,'min_samples_split': 5})
				model = ensemble.GradientBoostingClassifier(**params)
			elif self.name=="ADAB":model=AdaBoostClassifier(DecisionTreeClassifier(max_depth=1),algorithm="SAMME",n_estimators=200)
			elif self.name=="REG":model=LogisticRegression(random_state=0, solver='lbfgs',multi_class='multinomial')
			elif self.name=="SVM":model=SVC(gamma=2,C=1,probability=True)
			elif self.name=="RF":model=RandomForestClassifier(criterion='entropy', max_depth=15, min_samples_leaf=5,n_jobs=-1)
			elif self.name=="VC":
				clf1=LogisticRegression(random_state=0, solver='lbfgs',multi_class='multinomial')
				clfs=[]
				for i in range(len(self.X_train[0])):clfs+=[("lr"+str(i),clf1)]
				model = VotingClassifier(estimators=clfs, voting='soft')
			model.fit(self.X_train, self.y_train)
		return model
	def getData(self,filename,indexes,embeddingfile=None):
		if embeddingfile:wtranse=KeyedVectors.load_word2vec_format(embeddingfile, binary=True,unicode_errors="ignore")
		with open(filename) as f:data=[i.split("\t") for i in f.read().split("\n") if len(i)>2]
		xtrain,ytrain=[],[]
		for i in data:
			if len(i)<7:continue
			elif len(i)==7:sim,rel,meanErel,m1,m2,m3,y=i
			elif len(i)==11:mention,cname,midc,mid,sim,rel,meanErel,m1,m2,m3,y=i
			elif len(i) ==12:mention,cname,midc,mid,sim,rel,meanErel,m11,m12,m13,typ,y=i;meanErel2,m21,m22,m23=0,0,0,0;meanErel3,m31,m32,m33=0,0,0,0;meanErel4,m41,m42,m43=0,0,0,0
			elif len(i) ==14:mention,cname,midc,mid,sim,rel,meanErel,m11,m12,m13,_,_,typ,y=i;meanErel2,m21,m22,m23=0,0,0,0;meanErel3,m31,m32,m33=0,0,0,0;meanErel4,m41,m42,m43=0,0,0,0
			elif len(i)==16:mention,cname,midc,mid,sim,rel,meanErel,m11,m12,m13,meanErel2,m21,m22,m23,typ,y=i;meanErel3,m31,m32,m33=0,0,0,0;meanErel4,m41,m42,m43=0,0,0,0
			elif len(i)==20:mention,cname,midc,mid,sim,rel,meanErel,m11,m12,m13,meanErel2,m21,m22,m23,meanErel3,m31,m32,m33,typ,y=i;meanErel4,m41,m42,m43=0,0,0,0
			elif len(i)==24:mention,cname,midc,mid,sim,rel,meanErel,m11,m12,m13,meanErel2,m21,m22,m23,meanErel3,m31,m32,m33,meanErel4,m41,m42,m43,typ,y=i
			else:print(i);exit()
			#if midc=="NIL":continue
			try:sim=1 - spatial.distance.cosine(wtranse["_".join(cname.lower().split(" "))],wtranse["_".join(mention.lower().split(" "))])
			except:pass
			#if sum(map(float,[meanErel,m1,m2,m3]))>0:x1,x2,x3,x4=0,0,0,0
			features=map(float,[sim,rel,meanErel,m11,m12,m13,meanErel2,m21,m22,m23,meanErel3,m31,m32,m33,meanErel4,m41,m42,m43,typ])
			features=[features[l]for l in indexes]
			xtrain+=[features]
			ytrain+=[[int(i[-1]=='1')]]
		return xtrain,ytrain
			
				
	def updateabrdic(self):
		with open("/nfs/nas4/cicoda_tmp/cicoda_tmp/entitylinking/accronymes.csv") as f:tmp=f.read().split("\n")
		abrdic={}
		for i in tmp:j=i.split(";");abrdic[j[0]]=j[1:]	
		return abrdic
	def predict(self,filename,outputfile="tac2018runs/tac2017.run"):
		with open(filename) as f:data=[i.split("\t") for i in f.read().split("\n")]
		abrdic=self.updateabrdic()
		indexes=self.features
		predictions=[]
		probas=[]#probas to compute nDCG
		y_true, y_pred=[],[]
		y_nt, y_np=[],[]
		first=True
		nbmention=0
		found=0;links=0;tt=0;nils=0
		for i in data:
			if "Mention" in i:
				if first:
					first=False
					pmin,pmax,maxtarget,mintarget=0,0,None,None
					continue
				if maxtarget==None and mintarget==None:target="NIL"+"{0:0=5d}".format(v2.value);v2.value+=1
				elif maxtarget!=None:target=maxtarget
				else:target=mintarget
				"""if maxtarget==None :target="NIL"+"{0:0=5d}".format(v2.value);v2.value+=1
				else:target=maxtarget"""
				try:target=abrdic[mention.lower()][-1]
				except:pass
				#try:s,idd,m,docid,mid,et,nt,p=dataset[nbmention]
				#except:print(dataset[nbmention]);exit()
				#predictions+=["\t".join([s,idd,m,docid,target,et,nt,p])]
				try:
					if "m." in mid:
						y_pred+=[target];y_true+=[mid];tt+=1
						y_nt+=[cname]; y_np+=[mention]
						if "m." in target:links+=1
						if target==mid :found+=1
						#else:print("\t".join([mention,cname,target,mid]))
				except:pass
				probas+=[pmax]
				pmin,pmax,maxtarget,mintarget=0,0,None,None;mid=None
				nbmention+=1
			#print(len(i))
			if len(i)<7:continue
			elif len(i)==7:sim,rel,meanErel,m1,m2,m3,y=i
			elif len(i)==11:mention,cname,midc,mid,sim,rel,meanErel,m1,m2,m3,y=i
			elif len(i) ==12:mention,cname,midc,mid,sim,rel,meanErel,m11,m12,m13,typ,y=i;meanErel2,m21,m22,m23=0,0,0,0;meanErel3,m31,m32,m33=0,0,0,0;meanErel4,m41,m42,m43=0,0,0,0
			elif len(i) ==14:mention,cname,midc,mid,sim,rel,meanErel,m11,m12,m13,_,_,typ,y=i;meanErel2,m21,m22,m23=0,0,0,0;meanErel3,m31,m32,m33=0,0,0,0;meanErel4,m41,m42,m43=0,0,0,0
			elif len(i)==16:mention,cname,midc,mid,sim,rel,meanErel,m11,m12,m13,meanErel2,m21,m22,m23,typ,y=i;meanErel3,m31,m32,m33=0,0,0,0;meanErel4,m41,m42,m43=0,0,0,0
			elif len(i)==20:mention,cname,midc,mid,sim,rel,meanErel,m11,m12,m13,meanErel2,m21,m22,m23,meanErel3,m31,m32,m33,typ,y=i;meanErel4,m41,m42,m43=0,0,0,0
			elif len(i)==24:mention,cname,midc,mid,sim,rel,meanErel,m11,m12,m13,meanErel2,m21,m22,m23,meanErel3,m31,m32,m33,meanErel4,m41,m42,m43,typ,y=i
			else:print(i);exit()
			#if midc=="NIL":continue
			try:sim=1 - spatial.distance.cosine(wtranse["_".join(cname.lower().split(" "))],wtranse["_".join(mention.lower().split(" "))])
			except:pass
			#if sum(map(float,[meanErel,m1,m2,m3]))>0:x1,x2,x3,x4=0,0,0,0
			features=map(float,[sim,rel,meanErel,m11,m12,m13,meanErel2,m21,m22,m23,meanErel3,m31,m32,m33,meanErel4,m41,m42,m43,typ])
			features=[features[l]for l in indexes]
			
			if not typ:continue
			if self.name=="KERAS":
				yp=self.model.predict_classes(np.array([features]))[0][0]
				p=self.model.predict(np.array([features]))
				fp=1-p
			else:
				yp=self.model.predict(np.array([features]))[0]
				p,fp=self.model.predict_proba(np.array([features]))[0]
			
			if yp and p>pmax:maxtarget=midc;pmax=p
			#if not yp and fp>pmin:mintarget=midc;pmin=fp
		p,r=100*float(found)/max(1,links),100*float(found)/max(1,tt)
		un=len([i for i in y_pred])
		intt=0
		#with open("notfound.0.1.txt") as f:y0=f.read().split()
		for i in y_true:
			if i not in y_pred:un+=1
			if i in y_pred:intt+=1
		#print(len(y_true),len(y_pred))
		y=[]
		for i in range(len(y_true)):
			if y_true[i]!=y_pred[i]:y+=[y_np[i]+":"+y_nt[i]+":"+y_true[i]+":"+y_pred[i]]
		y.sort()
		output=None
		"""if len(indexes)==6:output="exp15.notfound.p1"
		elif len(indexes)==10:output="exp15.notfound.p1.p2"
		elif len(indexes)==11:output="exp15.notfound.p1.p2.p3.sum" 
		if output!=None:
			with open(output,"w") as f:f.write("\n".join(y))
		print(len(y))"""
		#print("Accuracy ",100*intt/un)
		#print(mapk(y_true,y_pred))
		#print(precision_score(y_true,y_pred,average="macro"))
		#print(recall_score(y_true,y_pred,average="macro"))
		#print("GINI ###########  "+self.gini(y_true,y_pred)+ " ###########")
		pi,ri,fi,_=precision_recall_fscore_support(y_true,y_pred,average="micro",labels=np.unique(y_true))
		pa,ra,fa,_=precision_recall_fscore_support(y_true,y_pred,average="macro",labels=np.unique(y_true))
		#print("f1-micro\t"+str(fi))
		#print("f1-macro\t"+str(fa))
		#print("f1-score\t"+str(2*p*r/(p+r)))
		print(2*p*r/(p+r))
		
	def gini(self,actual, pred):
		assert (len(actual) == len(pred))
		dic={}
		j=0
		for i in set(actual+pred):dic[i]=j;j+=1
		actual, pred=[dic[i] for i in actual],[dic[i] for i in pred]
		all = np.asarray(np.c_[actual, pred, np.arange(len(actual))], dtype=np.float)
		all = all[np.lexsort((all[:, 2], -1 * all[:, 1]))]
		totalLosses = all[:, 0].sum()
		giniSum = all[:, 0].cumsum().sum() / totalLosses
		giniSum -= (len(actual) + 1) / 2.
		return giniSum / len(actual)

	def gini_normalized(self,actual, pred):
		return gini(actual, pred) / gini(actual, actual)
			

class Tests():
	def __init__(self, name,trainfile,testfile,validfile,embeddingfile=None):
		self.name=name
		self.trainfile,self.testfile,self.validfile=trainfile,testfile,validfile
		self.embeddingfile=embeddingfile
	def OneShot(self,indexes):
		labels=[0,1]
		names=["Sim","Rel","sum1","max@11","max@12","max@13","sum2","max@21","max@22","max@23","sum3","max@31","max@32","max@33","types"]
		#print("################### " +" + ".join([names[i]for i in indexes])+"  ################")
		Mclf=Classifier(self.name,self.trainfile,self.testfile,self.validfile,indexes,self.embeddingfile)
		Mclf.predict(self.testfile)
		clf=Mclf.model
		#if self.name=="REG":print(clf.coef_)
		#if self.name=="KERAS":yp=clf.predict_classes(np.array(Mclf.X_test))
		#else:yp=clf.predict(np.array(Mclf.X_test));yp=[[i] for i in yp]
		
		#y_probas = clf.predict_proba(np.array(Mclf.X_test))
		#y_probas=[i[0] for i in y_probas]
		#skplt.metrics.plot_cumulative_gain(np.array(Mclf.y_test), y_probas)
		#skplt.metrics.plot_precision_recall_curve(np.array(Mclf.y_test), y_probas)
		#skplt.metrics.plot_lift_curve(np.array(Mclf.y_test), y_probas)
		"""fpr, tpr, thresholds = roc_curve(np.array(Mclf.y_test), [i[1] for i in Mclf.X_test])
		roc_auc = auc(fpr, tpr)
		print(roc_auc)
		plt.title('ROCceiver Operating Characteristic)')
		plt.plot(fpr, tpr, 'b',
		label='AUC = %0.4f'% roc_auc)
		plt.legend(loc='lower right')
		plt.plot([0,1],[0,1],'r--')
		plt.xlim([-0.1,1.2])
		plt.ylim([-0.1,1.2])
		plt.ylabel('True Positive Rate (TPR)')
		plt.xlabel('False Positive Rate (FPR)')"""
		#plt.savefig("precision_recall"+".".join([names[i]for i in indexes])+".png")
		#plt.cla()
		
		#print(metrics.flat_classification_report(np.array(Mclf.y_test), yp, labels=labels, digits=3))
		#print("rock it",average_precision_score(np.array(Mclf.y_test),y_probas))
		#gini=1-sum([i*i for i in y_probas])
		"""if indexes in [[1],[0,1],[0,1,2],[0,1,2,3],[0,1,3,4,5],[0,1,2,3,4,5]]:
			#plt.savefig("roc"+".".join([names[i]for i in indexes])+".png")"""
		#if "aida" in self.trainfile: dump(clf, self.name+".linker1."+".".join([names[i]for i in indexes])+'.joblib')
		#else:dump(clf, self.name+".linker1."+".".join([names[i]for i in indexes])+'.joblib')
		
	def generateAll(self,indexes):
		for i in indexes:#chain(*(combinations(indexes, i) for i in range(len(indexes) + 1))):
			self.OneShot(i)
	def MST(self):
		with open(self.testfile) as f:data=[i.split("\t") for i in f.read().split("\n")]
		mst,cands,tmst,tcands=[],[],[],[]
		for i in data:
			if "DOC" in i:
				if len(tmst)>0:
					x=tmst[:3]
					if len(x)<3:x+=[0]*(3-len(x))
					mst+=[x];cands+=[tcands[:3]]
				if len(mst)!=0:
					print(mst,cands)
					mst=np.asmatrix(mst)
					print(mst.shape)
					Tcsr = minimum_spanning_tree(mst)
					print(Tcsr.toarray().astype(int))
				mst,cands=[],[]
			if "Mention" in i:
				if len(tmst)>0:
					x=tmst[:3]
					if len(x)<3:x+=[0]*(3-len(x))
					mst+=[x];cands+=[tcands[:3]]
				tmst,tcands=[],[]
			if len(i)<7:continue
			elif len(i)==7:sim,rel,merel,m1,m2,m3,y=i
			elif len(i)==11:mention,cname,midc,mid,sim,rel,merel,m1,m2,m3,y=i
			elif len(i) ==12:mention,cname,midc,mid,sim,rel,merel,m1,m2,m3,typ,y=i
			else:print(i);exit()
			tmst+=[float(merel)]
			tcands+=[(midc,mid)]

import argparse
parser = argparse.ArgumentParser(description='Description of your program')
parser.add_argument('-tr','--train', help='training file',nargs='?',const=False, required=True)
parser.add_argument('-te','--test', help='test file',nargs='?',const=False, required=True)
parser.add_argument('-o','--one', help='one shot?',nargs='?',const=False, required=False)
parser.add_argument('-c','--classifier', help='classifier type',nargs='?',const=False, required=True)
parser.add_argument('-f','--features', help='features to consider in teh training',nargs='?',const=False, required=False)
args = vars(parser.parse_args())

one=1
if args["one"]:one=int(args["one"])
if args["train"]:train=args["train"]
if args["test"]:test=args["test"]
if args["classifier"]:classtype=args["classifier"]

if args["features"]:features=args["features"];indexes=features.split(",")
else:one=0
if classtype=="KERAS":
	from keras.models import Sequential
	from keras.layers import Dense
	from keras.wrappers.scikit_learn import KerasClassifier
	from sklearn.model_selection import cross_val_score
	from sklearn.preprocessing import LabelEncoder
	from sklearn.model_selection import StratifiedKFold
	from sklearn.preprocessing import StandardScaler
	from sklearn.pipeline import Pipeline
	from keras import backend as K
	from keras.layers import Dropout

exp=Tests(classtype,train,test,None)
#exp.MST()
allfeatures=[[0,1],[0,1,2,3,4,5],[0,1,2,3,4,5,6],[0,1,2,3,4,5,7,8,9],[0,1,2,3,4,5,6,7,8,9],[0,1,2,3,4,5,6,7,8,9,10],[0,1,2,3,4,5,6,7,8,9,11,12,13],[0,1,2,3,4,5,6,7,8,9,10,11,12,13]]
#print(map(int,indexes));exit()
if one:exp.OneShot(map(int,indexes))#range(features))
else:exp.generateAll(allfeatures)
#"/nfs/nas4/cicoda_tmp/cicoda_tmp/word2vec/trunk/models/skipgram.s100.w5.enwikianchor15G.cleaned.bin"
#exp.OneShot([0,1,2])#0,1,2])#0,1,2])#,3,4,5])
#makeExp("aida-yago2-dataset/aida.exp0.train.tsv","aida-yago2-dataset/aida.exp.test.tsv","aida-yago2-dataset/aida.exp.valid.tsv",[0,1,2,3,4,5])
#(97.48255234297109, 81.76876437382396, 88.9368959636157)aidaA
#(97.50919898103595, 76.93166592228674,85.9)aidaB
#http://gerbil.aksw.org/gerbil/experiment?id=201902280007 : gerbil exps
#http://gerbil.aksw.org/gerbil/experiment?id=201902280009
######### http://gerbil.aksw.org/gerbil/experiment?id=201904020001 : gerbil exp 02/04/2019
######## http://gerbil.aksw.org/gerbil/experiment?id=201904020002	 : gerbil exp2

#python classification.py -tr tac16.exp6.eval.tsv -te tac17.exp6.tsv -c REG

""">>> entitiesInCross("aida-yago2-dataset/aida.train.tsv")
('in croswiki', 14865, 14370, 18539, 80, 77)
>>> entitiesInCross("aida-yago2-dataset/aida.test.tsv")
('in croswiki', 3574, 3476, 4483, 79, 77)
>>> entitiesInCross("aida-yago2-dataset/aida.valid.tsv")
('in croswiki', 4046, 3949, 4788, 84, 82)
>>> entitiesInCross("ENG2017_EVAL.ids",False)
('in croswiki', 5703, 3387, 6915, 82, 48)

('m.06y3r', '<http://rdf.basekb.com/ns/user.mdaconta.human_resources.employee.employedby>', 'm.0k8z')
('m.06y3r', '<http://rdf.basekb.com/ns/organization.organization_founder.organizations_founded>', 'm.0k8z')
('m.0k8z', '<http://rdf.basekb.com/ns/organization.organization.founders>', 'm.06y3r')

###46 new discovered
"""
