#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -W ignore
import os,sys,time,math
import wikipedia
import numpy as np
import pandas as pd
from tqdm import tqdm
import concurrent.futures
from multiprocessing import Pool
import time
import requests
import gc

def mappings(dicname,output):
	print("dic to nums ids")
	mid2numsdic={}
	dic=np.load(dicname,allow_pickle=True).item()
	j=0
	for i in dic:
		s,o=i

		try:_=mid2numsdic[s]
		except:mid2numsdic[s]=j;j+=1
		try:_=mid2numsdic[o]
		except:mid2numsdic[o]=j;j+=1
	np.save(output,mid2numsdic)
	print("entities : ",j)

def mappingsFromDic(inputdic,rootdic,output):
	print("dic to nums ids")
	mid2numsdic={}
	dic=np.load(inputdic,allow_pickle=True).item()
	root=np.load(rootdic,allow_pickle=True).item()
	j=len(root)
	print("root entities : ",j)
	for i in dic:
		for o in i:
			try:mid2numsdic[o]=root[o]
			except:
				try:_=mid2numsdic[o]
				except:j+=1;mid2numsdic[o]=j
	print("all entities : ",j)
	np.save(output,mid2numsdic)
	
def readTriples(filename):
	with open(filename) as f:data=f.read().split("\n")
	print(len(data))
	lines=[]
	comments=0
	for i in data:
		if "#@"== i[:2]:comments+=1
		if len(i)==0 or i[0]=="@":lines+=[i];continue
		try:s,r,o,_=i.split();lines+=[i]
		except:continue
	with open("/nfs/nas4/cicoda/cicoda/yagoFacts.clean.ttl","w") as f:f.write("\n".join(lines)) 
	print(len(lines))
	print(comments)
def removeType(filename):
		with open(filename) as f:data=f.read().split("\n")
		print(len(data))
		lines=[]
		for i in data:
			if "rdf:type" not in i:lines+=[i]
		print(len(lines))
		with open("/nfs/nas4/cicoda/cicoda/yagoFacts.notype.ttl","w") as f:f.write("\n".join(lines)) 
def getWiki(name):

		try:res=wikipedia.page(name.replace("_"," ")).pageid
		except:res=None
		return res
		
def getWiki2(name):
		try:
				res = requests.get(url='https://en.wikipedia.org/w/api.php?action=query&prop=pageprops&titles='+name+'&format=json').json()
				pageid=res["query"]["pages"].keys()[0]
				#wikidataid=res["query"]["pages"][pageid]["pageprops"]["wikibase_item"]
				#print(wikidataid)
		except:pageid=None
		return pageid

def parallele(function,tab):
	predictions=[]
	executor = concurrent.futures.ProcessPoolExecutor(40)
	futures = [executor.submit(function, group) for group in tab]#
	concurrent.futures.wait(futures)
	for i in futures:predictions+=i.result()
	return predictions
		
def yago2wikipedia(dicname):
		dic=np.load(dicname,allow_pickle=True).item()
		l=[]
		j=0
		mid2numsdic=np.load("Dicts/mid2numsdic.npy",allow_pickle=True).item()
		print("docs loaded")
		"""for i in dic:
					try:s,o=i;l+=[s];l+=[o]
					except:print(i);exit()
		l=list(set(l))"""
		l=dic.values()
		keys=dic.keys()
		none=0
		print(len(l))
		#res=parallele(getWiki,l[:10])
		p = Pool(50)
		res=p.map(getWiki2, l)
		yid2wid={}
		for i in range(len(res)):
			if res[i]==None:none+=1
			yid2wid[keys[i]]=res[i]
		print(none)
		print(len(yid2wid))
		np.save("Dicts/yid2wid.npy",yid2wid)	

def label2id(labelsFile):
			mid2numsdic=np.load("Dicts/mid2numsdic.npy",allow_pickle=True).item()
			print("docs loaded")
			id2label={}
			n=0
			urls=0
			with open(labelsFile) as f:data=f.read().split("\n")
			for i in data:
				try:s,r,o=i.split("\t")
				except:continue
				if r !="<hasWikipediaUrl>":continue
				else:urls+=1
				try:
					s=s[1:-1]
					_=mid2numsdic[s]
					id2label[s]=o[30:-3]
				except:n+=1;pass
			print(len(id2label))
			print(id2label.keys()[:10])
			print(id2label.values()[:10])
			np.save("Dicts/yago.id2label.npy",id2label)
			print(n,urls)
def getWiki3(yid2wid):
	nones=[i for i in yid2wid if yid2wid[i]==None]
	samples=min(2000,len(nones))
	if not samples:exit()
	#print(nones)
	p = Pool(50)
	res=p.map(getWiki2, nones[:samples])
	p.terminate()
	p.join()
	for i in range(samples):yid2wid[nones[i]]=res[i]
	n=len([i for i in yid2wid if yid2wid[i]==None])
	print("remining : ",n)
	#np.save(name,yid2wid)
	return yid2wid
	
def wikipedia2yago(name,output):
		yid2wid=np.load(name,allow_pickle=True).item()
		tmp={}
		for i in yid2wid:tmp[yid2wid[i]]=i
		np.save(output,yid2wid)
def wikipedia2yagoNum(name,output):
		mid2numsdic=np.load("Dicts/YagoSaturated.mid2numsdic.npy",allow_pickle=True).item()
		yid2wid=np.load(name,allow_pickle=True).item()
		tmp={}
		for i in yid2wid:
			if yid2wid[i]:tmp[yid2wid[i]]=mid2numsdic[i]
		np.save(output,tmp)
		print("saved entities : ",len(tmp))
def computeYagoTowiki(name,output):
	yagosNum=np.load(name,allow_pickle=True).item()
	yago2wid=np.load("Dicts/yid2wid.npy",allow_pickle=True).item()
	yid2wid={}
	print("in yagos :",len(yagosNum))
	p=0
	for i in yagosNum:
		try:yid2wid[i]=yago2wid[i]
		except:yid2wid[i]=None;p+=1
	print("yid not found : ",len([i for i in yid2wid if yid2wid[i]==None]))
	for i in range(100):yid2wid=getWiki3(yid2wid)
	np.save(output,yid2wid)
	print("yid not found : ",len([i for i in yid2wid if yid2wid[i]==None]))
	
def wid2num(name_yid2num,name_yid2wid,output):
	yid2num=np.load(name_yid2num,allow_pickle=True).item()
	yid2wid=np.load(name_yid2wid,allow_pickle=True).item()
	dic={}
	for i in yid2wid:dic[yid2wid[i]]=yid2num[i]
	np.save(output,dic)
	
	
def computeCos(filename):
	with open(filename) as f:data=f.read().split("\n")
	lines=[]
	for i in data:
		tmp=i.split("\t")
		if len(tmp)>2:mention,cname=tmp[0],tmp[1]
		else:lines+=[i];continue
		try:sim=1 - spatial.distance.cosine(wtranse["_".join(cname.lower().split(" "))],wtranse["_".join(mention.lower().split(" "))])
		except:sim=0
		tmp[4]=str(sim)
		lines+=["\t".join(tmp)]
	with open(filename,"w") as f:f.write("\n".join(lines))
	
def allCands(docs):
	output="data/YagoSaturated.tac.exp15.cands.tsv"
	entities=[]
	root="/nfs/nas4/cicoda_tmp/cicoda_tmp/entitylinking/"
	mids2wids=np.load(root+"basekb.mids2wids.npy",allow_pickle=True).item()
	wid2yid=np.load("Dicts/YagoSaturated.wid2yidnum.npy",allow_pickle=True).item()
	print(mids2wids.keys()[:2])
	print(wid2yid.keys()[:2])
	for filename in docs:
		with open(filename) as f:data=[i.split("\t") for i in f.read().split("\n") if len(i)>2]
		for i in data:
			if len(i)<6:continue
			elif len(i)==13:mention,cname,mid1,mid2,sim,rel,merel,merel2,m1,m2,m3,typ,y=i
			elif len(i) ==12:mention,cname,midc,mid,sim,rel,meanErel,m11,m12,m13,typ,y=i;meanErel2,m21,m22,m23=0,0,0,0;meanErel3,m31,m32,m33=0,0,0,0
			elif len(i) ==14:mention,cname,midc,mid,sim,rel,meanErel,m11,m12,m13,_,_,typ,y=i;meanErel2,m21,m22,m23=0,0,0,0;meanErel3,m31,m32,m33=0,0,0,0
			elif len(i)==16:mention,cname,midc,mid,sim,rel,meanErel,m11,m12,m13,meanErel2,m21,m22,m23,typ,y=i;meanErel3,m31,m32,m33=0,0,0,0
			elif len(i)==20:mention,cname,mid1,mid2,sim,rel,meanErel,m11,m12,m13,meanErel2,m21,m22,m23,meanErel3,m31,m32,m33,typ,y=i
			try:
				idd=wid2yid[mids2wids[mid1]]
				entities+=[str(idd)]
			except:pass
			try:
				idd=wid2yid[mids2wids[mid2]]
				entities+=[str(idd)]
			except:pass
		print(len(entities))
	print(output)
	with open(output,"w") as f:f.write("\n".join(list(set(entities))))

def hamacher(x,y,p,op):
	if op=="n":return x*y/(p+(1-p)*(x+y-x*y))
	elif op=="c":return (x+y-x*y-(1-p)*x*y)/(1-(1-p)*x*y)
	
def hamacher2(l,p,op):
	res=l[0]
	for i in range(1,len(l)):res=hamacher(l[i],res,p,op)
	return res

def csv2dic(filename,agg="yeger",p=1):
	if "bz2" in filename:compression="bz2"
	else:compression="infer"
	df=pd.read_csv(filename,header=None,chunksize=10000000,compression=compression)
	dic={}
	print("building dic with agg="+agg+" and p="+str(p))
	for df_chunk in tqdm(df):
		s=list(df_chunk[0])
		o=list(df_chunk[1])
		wsrm=[list(df_chunk[i]) for i in range(2,len(df_chunk.columns))]
		for i in range(len(s)):
			weights=[wsrmi[i] for wsrmi in wsrm]
			if agg=="yeger":m=yeger2(weights,p,"n")#t-norm =ET
			elif agg=="hamacher":m=hamacher2(weights,p,"n")
			elif agg=="einstein":m=einstein2(weights,"n")
			elif agg=="minmax":m=min(weights)
			elif agg=="owalike":m=OWAlike(weights,p,"n")
			elif agg=="badd":m=BADD_OWA(weights,p,"n")
			elif agg=="cheikh":m=cheikh2(weights,p,"n")
			elif agg=="drastic":m=drastic2(weights,"n")
			elif agg=="compensatory":m=compensatory(hamacher2,weights,p)
			else:print("operator ",agg," not defined excited ***");exit()
			try:
				if agg=="yeger":dic[(s[i],o[i])]=yeger(dic[(s[i],o[i])],m,p,"c")
				elif agg=="hamacher":dic[(s[i],o[i])]=hamacher(dic[(s[i],o[i])],m,p,"c")
				elif agg=="einstein":dic[(s[i],o[i])]=einstein(dic[(s[i],o[i])],m,"c")
				elif agg=="minmax":dic[(s[i],o[i])]=max(dic[(s[i],o[i])],m)
				elif agg=="owalike":dic[(s[i],o[i])]=OWAlike([dic[(s[i],o[i])],m],p,"c")
				elif agg=="badd":dic[(s[i],o[i])]=BADD_OWA([dic[(s[i],o[i])],m],p,"c")
				elif agg=="cheikh":dic[(s[i],o[i])]=cheikh2([dic[(s[i],o[i])],m],p,"c")
				elif agg=="drastic":m=drastic(dic[(s[i],o[i])],m,"c")
				elif agg=="compensatory":dic[(s[i],o[i])]=compensatory(hamacher2,[dic[(s[i],o[i])],m],p)
				else:print("operator ",agg," not defined excited ***");exit()
			except:dic[(s[i],o[i])]=m
	r,b=os.path.splitext(filename)
	if p==None:output=r+"."+agg+".npy"	
	else:output=r+"."+agg+"."+str(p)+".npy"	
	np.save(output, dic)
	#dicDistrib(output)

def csv2dic2(dirname,paths=["oneedgepaths.csv","twoedgespaths.csv","threeedgespaths.csv"]):
	dfs=[]
	agg="hamacher";p=0
	pnames=["one","two","three","four","five"]
	for filename in paths:
		if "bz2" in filename:compression="bz2"
		else:compression="infer"
		dfs+=[pd.read_csv(dirname+filename,header=None,chunksize=10000000,compression=compression)]
	dic={}
	print("building dic with p="+str(p))
	for df in dfs:
		for df_chunk in tqdm(df):
			s=list(df_chunk[0])
			o=list(df_chunk[1])
			wsrm=[list(df_chunk[i]) for i in range(2,len(df_chunk.columns))]
			for i in range(len(s)):
				weights=[wsrmi[i] for wsrmi in wsrm]
				if agg=="yeger":m=yeger2(weights,p,"n")#t-norm =ET
				elif agg=="hamacher":m=hamacher2(weights,p,"n")
				else:print("operator ",agg," not defined excited ***");exit()
				try:
						if agg=="yeger":dic[(s[i],o[i])]=yeger(dic[(s[i],o[i])],m,p,"c")
						elif agg=="hamacher":dic[(s[i],o[i])]=hamacher(dic[(s[i],o[i])],m,p,"c")
						else:print("operator ",agg," not defined excited ***");exit()
				except:dic[(s[i],o[i])]=m
	print("saving and plotting final dic ...")
	output=dirname+str(pnames[len(paths)-1])+"."+agg+"."+str(p)+".type2.npy"		
	np.save(output, dic)
	#dicDistrib(output)
	
def social_proximity1(dirname,paths=["oneedgepaths.csv","twoedgespaths.csv","threeedgespaths.csv"],y=1.5):
	dfs=[]
	agg="hamacher";p=0
	pnames=["one","two","three","four","five"]
	for filename in paths:
		if "bz2" in filename:compression="bz2"
		else:compression="infer"
		dfs+=[pd.read_csv(dirname+filename,header=None,chunksize=10000000,compression=compression)]
	dic={}
	print("building dic with alambda="+str(y))
	for df in dfs:
		for df_chunk in tqdm(df):
			s=list(df_chunk[0])
			o=list(df_chunk[1])
			wsrm=[list(df_chunk[i]) for i in range(2,len(df_chunk.columns))]
			for i in range(len(s)):
				weights=[wsrmi[i] for wsrmi in wsrm]
				if agg=="yeger":m=yeger2(weights,p,"n")#t-norm =ET
				elif agg=="hamacher":m=hamacher2(weights,p,"n")
				elif agg=="einstein":m=einstein2(weights,"n")
				elif agg=="minmax":m=min(weights)
				elif agg=="owalike":m=OWAlike(weights,p,"n")
				elif agg=="badd":m=BADD_OWA(weights,p,"n")
				elif agg=="cheikh":m=cheikh2(weights,p,"n")
				elif agg=="drastic":m=drastic2(weights,"n")
				elif agg=="compensatory":m=compensatory(hamacher2,weights,p)
				else:print("operator ",agg," not defined excited ***");exit()
				try:dic[(s[i],o[i])]+=m/(y**(len(weights)-2))
				except:dic[(s[i],o[i])]=m/(y**(len(weights)-2))
	print("normalizing dic ...")
	if y>1:
		Cy=(y-1)/y
		for i in dic:dic[i]=Cy*dic[i]
	print("saving and plotting final dic ...")
	output=dirname+"/Prox1/"+str(pnames[len(paths)-1])+"."+agg+"."+str(p)+".prox1."+str(y)+".npy"		
	np.save(output, dic)
	#dicDistrib(output)
	
def social_proximity2(dirname,paths=["oneedgepaths.csv","twoedgespaths.csv","threeedgespaths.csv"],coefs=[1,0,0,0,0]):
	dfs=[]
	pnames=["one","two","three","four","five"]
	agg="hamacher";p=0
	for filename in paths:
		if "bz2" in filename:compression="bz2"
		else:compression="infer"
		dfs+=[pd.read_csv(dirname+filename,header=None,chunksize=10000000,compression=compression)]
	dic={}
	print("building dic with coefs="+str(coefs[len(paths)-1]))
	pp=0
	for df in dfs:
		for df_chunk in tqdm(df):
			s=list(df_chunk[0])
			o=list(df_chunk[1])
			wsrm=[list(df_chunk[i]) for i in range(2,len(df_chunk.columns))]
			for i in range(len(s)):
				weights=[wsrmi[i] for wsrmi in wsrm]
				if agg=="yeger":m=yeger2(weights,p,"n")#t-norm =ET
				elif agg=="hamacher":m=hamacher2(weights,p,"n")
				elif agg=="einstein":m=einstein2(weights,"n")
				elif agg=="minmax":m=min(weights)
				elif agg=="owalike":m=OWAlike(weights,p,"n")
				elif agg=="badd":m=BADD_OWA(weights,p,"n")
				elif agg=="cheikh":m=cheikh2(weights,p,"n")
				elif agg=="drastic":m=drastic2(weights,"n")
				elif agg=="compensatory":m=compensatory(hamacher2,weights,p)
				else:print("operator ",agg," not defined excited ***");exit()
				try:dic[(s[i],o[i])]+=m*coefs[pp]
				except:dic[(s[i],o[i])]=m*coefs[pp]
		pp+=1
	print("saving dic ...")
	output=dirname+"/Prox2/"+str(pnames[len(paths)-1])+"."+agg+"."+str(p)+".prox2."+str(coefs[len(paths)-1])+".npy"	
	np.save(output, dic)
	#dicDistrib(output)
	
def dic2csv(filename,output):
	wsrm=np.load(filename,allow_pickle=True).item()
	mid2numsdic=np.load("Dicts/YagoSaturated.mid2numsdic.npy",allow_pickle=True).item()
	res=[]
	for i in wsrm:res+=[",".join(map(str,[mid2numsdic[i[0]],mid2numsdic[i[1]],wsrm[i]]))]
	with open(output,"w")as f:f.write("\n".join(res))

def updateonepath(filename):
	with open(filename) as f:data=f.read().split("\n")
	lines=[]
	for i in data:
		tmp=i.split("\t")
		if len(tmp)>2:mention,cname=tmp[0],tmp[1]
		else:lines+=[i];continue
		tmp[4]=str(sim)
		lines+=["\t".join(tmp)]
	with open(filename,"w") as f:f.write("\n".join(lines))
def updatetwopath(filename):return None
def updatethreepath(filename):return None

def paralellSP1(i):social_proximity1(i[0],paths=i[1],y=i[2])
def parallalizeSP1():
	paths=["oneedgepaths.csv","twoedgespaths.csv","threeedgespaths.csv","fouredgespaths.csv.bz2"]
	for ps in  [[paths[0]],[paths[0],paths[1]],[paths[0],paths[1],paths[2]]]:#,[paths[0],paths[1],paths[2],paths[3]]]:
			p = Pool(5)
			params=[("/nfs/nas4/cicoda_tmp/cicoda_tmp/licoda/data/YagoSaturated/",ps,_y) for _y in [1.01,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2.0]]
			res=p.map(paralellSP1, params)
			p.terminate()
			p.join()

def paralellSP2(i):social_proximity2(i[0],paths=i[1],coefs=i[2])
def parallalizeSP2(path,edges):
	p = Pool(5)
	params=[(path,edges,[1,1,0.1,_y,0]) for _y in [0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]]
	res=p.map(paralellSP2, params)
	p.terminate()
	p.join()


t1=time.time()
##################################### WSRM ##############################################
#mappings("Dicts/YagoSaturated.wsrm.npy","Dicts/YagoSaturated.mid2numsdic.npy")
#computeYagoTowiki("Dicts/YagoSaturated.mid2numsdic.npy","Dicts/YagoSaturated.yid2wid.npy")
#wikipedia2yagoNum("Dicts/YagoSaturated.yid2wid.npy","Dicts/YagoSaturated.wid2yidnum.npy")
#dic2csv("Dicts/YagoSaturated.wsrm.npy","data/YagoSaturated.unarc.csv")
#docs=["datasets/TAC/tac16.exp15.yago.p1.eval.tsv","datasets/TAC/tac17.exp15.yago.p1.tsv"]
#allCands(docs)
##################################### Aggregations ##############################################
#csv2dic("/nfs/nas4/cicoda_tmp/cicoda_tmp/licoda/data/YagoSaturated/twoedgespaths.csv","hamacher",0)
#csv2dic("/nfs/nas4/cicoda_tmp/cicoda_tmp/licoda/data/YagoSaturated/threeedgespaths.csv","hamacher",0)
#csv2dic("/nfs/nas4/cicoda_tmp/cicoda_tmp/licoda/data/YagoSaturated/oneedgepaths.csv","hamacher",0)
#csv2dic("/nfs/nas4/cicoda_tmp/cicoda_tmp/licoda/data/YagoSaturated/fouredgespaths.csv.bz2","hamacher",0)

csv2dic2("/nfs/nas4/cicoda/cicoda/entitylinking/",["oneedgepaths.csv","twoedgespaths.csv"])
csv2dic2("/nfs/nas4/cicoda/cicoda/entitylinking/",["oneedgepaths.csv","twoedgespaths.csv","threeedgespaths.csv.bz2"])
#csv2dic2("/nfs/nas4/cicoda_tmp/cicoda_tmp/licoda/data/Yago/",["oneedgepaths.csv","twoedgespaths.csv","threeedgespaths.csv","fouredgespaths.csv.bz2"])
#parallalizeSP1()
#parallalizeSP2("data/YagoSaturated/",["oneedgepaths.csv","twoedgespaths.csv","threeedgespaths.csv","fouredgespaths.csv.bz2"])


print(time.time()-t1)

def preparePaths(wsrmdic):
		mappings(wsrmdic,"Dicts/YagoSaturated.mid2numsdic.npy")
		computeYagoTowiki("Dicts/YagoSaturated.mid2numsdic.npy","Dicts/YagoSaturated.yid2wid.npy")
		wikipedia2yagoNum("Dicts/YagoSaturated.yid2wid.npy","Dicts/YagoSaturated.wid2yidnum.npy")
		dic2csv("Dicts/YagoSaturated.wsrm.npy","data/YagoSaturated.unarc.csv")
		docs=["datasets/TAC/tac16.exp15.yago.p1.eval.tsv","datasets/TAC/tac17.exp15.yago.p1.tsv"]
		allCands(docs)
