package jena.examples.rdf ;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.IOException;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.*;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.sparql.util.QueryExecUtils;
import org.apache.jena.util.FileManager;
import org.apache.jena.vocabulary.ReasonerVocabulary;

//javac  -cp .:../apache-jena-3.12.0/lib/* jena/examples/rdf/Saturator.java
//java  -cp .:../apache-jena-3.12.0/lib/* jena.examples.rdf.Saturator

public class Saturator {

 
    //static final String inputFileName  = "/home/celvaigh/these/coursINSA/TP/celvaigh/JenaTutorial/src/jena/examples/rdf/vc-db-1.rdf";
    static final String ontology  = "/nfs/nas4/cicoda/cicoda/yagoSchema.ttl";
    static final String facts  = "/nfs/nas4/cicoda/cicoda/yagoFacts.clean.ttl";
    static String baseURI = "http://yago-knowledge.org/resource/";
    Model model;
    @SuppressWarnings("deprecation")
				public void write(String filename) {
								
						 	try {
						 		RDFDataMgr.write(new FileWriter(new File(filename)), model, RDFFormat.TURTLE_FLAT) ;
						 		//model.write(new FileWriter(new File(filename)),  "N-TRIPLES") ;
								} catch (IOException e) {
									// TODO Bloc catch généré automatiquement
									e.printStackTrace();
							 }
						 
				}
    
    public void build(String filename) {
    	model=RDFDataMgr.loadModel(filename) ;
    	//model = ModelFactory.createDefaultModel();
    	//model = FileManager.get().loadModel(filename,"<http://yago-knowledge.org/resource/>");
     //InputStream is = FileManager.get().open(filename);
     //model.read(is, null, "TTL");
    	model.setNsPrefix("ressource", baseURI);

    }
    
    public void infer() {
    	
   
    	model.add(FileManager.get().loadModel(ontology,"<http://yago-knowledge.org/resource/>"));
    	
    	//Reasoner reasoner = ReasonerRegistry.getRDFSReasoner();
    	//reasoner.setParameter(ReasonerVocabulary.PROPsetRDFSLevel, ReasonerVocabulary.RDFS_SIMPLE);
    	
    	String rules = "[rule1: (?p2 rdfs:subPropertyOf ?p1) (?x ?p2 ?y) -> (?x ?p1 ?y)]";
    	Reasoner reasoner = new GenericRuleReasoner(Rule.parseRules(rules));
    	reasoner.setDerivationLogging(true);	
    	InfModel infmodel = ModelFactory.createInfModel(reasoner, model );
    	model.add(infmodel);
    	
    }
    
    public void query(String queryString) {
    	/* Do a SPARQL Query over the data in the model */
    	//String queryString = "SELECT ?x ?z WHERE { ?x <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> ?z } limit 20" ;

    	// Now create and execute the query using a Query object 
    	Query query = QueryFactory.create("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+queryString) ;
    	QueryExecution qexec = QueryExecutionFactory.create(query, model) ;
    	ResultSet results = qexec.execSelect();
    	//QueryExecUtils.executeQuery(qexec);
    	ResultSetFormatter.out(System.out, results, query);
     qexec.close();
    	
    	//System.out.println( "\n----------\ndone" );
    }
			public static void main (String args[]) throws IOException {
				Saturator sat=new Saturator();
				sat.build(Saturator.facts);
				sat.query("SELECT (COUNT(?s) AS ?triples) WHERE { ?s ?p ?o. }");
				//sat.query("SELECT ?s ?o WHERE { ?s rdf:type ?o }");
				sat.infer();
				sat.query("SELECT (COUNT(?s) AS ?triples) WHERE { ?s ?p ?o. }");
				//sat.query("SELECT (COUNT(?s) AS ?triples) WHERE { ?s rdf:type ?o }");
				//sat.query("SELECT ?s ?p ?o WHERE { ?s ?p ?o FILTER NOT EXISTS {?s <http://www.w3.org/1999/02/22-rdf-syntax-ns#rdf:type> ?o}}");
				sat.write("/nfs/nas4/cicoda/cicoda/yagoSchemaSaturated.ttl");
				//sat.write("/nfs/nas4/cicoda/cicoda/yagoFactFromSaturator.ttl");
   }
}
